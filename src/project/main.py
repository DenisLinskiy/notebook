from object.user_story import UserStory
from object.notebook import NoteBook

if __name__ == "__main__":
    user_story = UserStory()
    user_story.show_title()
    user_story.show_notebook()
    while True:
        # --------------------------------------------------------------------------------------------------------------
        # Показати меню
        # --------------------------------------------------------------------------------------------------------------
        user_story.show_menu()

        # --------------------------------------------------------------------------------------------------------------
        # Користувач обирає пункт в меню
        # --------------------------------------------------------------------------------------------------------------
        try:
            choice = int(user_story.ask_user("Select Number: "))
        except ValueError:
            user_story.send_user("try again..., select NUMBER")
            continue

        # --------------------------------------------------------------------------------------------------------------
        # Перевірка вибору користувача
        # --------------------------------------------------------------------------------------------------------------
        if choice not in user_story.get_avail_items_in_menu():
            user_story.send_user("Wrong choice, try again...")
            continue

        # --------------------------------------------------------------------------------------------------------------
        # Виклик команди яку ввів користувач
        # --------------------------------------------------------------------------------------------------------------
        user_story.run_command(choice, NoteBook)
