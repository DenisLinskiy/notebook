import random


class HelpersFunction:
    def __init__(self):
        pass

    @staticmethod
    def wishlist():
        wishes = [
            "You will have good luck and overcome many hardships :)",
            "All your hard work will soon pay off :)",
            "Your loyalty is a virtue, but not when it's wedded with blind stubbornness :)",
            "Humor usually works at the moment of awkwardness :)",
            "Your first love has never forgotten you :0"
        ]
        return random.choice(wishes)


helpers = HelpersFunction()
