MENU = {
    1: {
        "name": "Add Record",
        "function": ""
    },
    2: {
        "name": "Remove Record",
        "function": ""
    },
    3: {
        "name": "Change Record",
        "function": ""
    },
    4: {
        "name": "Search by name",
        "function": ""
    },
    5: {
        "name": "Search by telephone number",
        "function": ""
    },
    6: {
        "name": "Search by surname first letter",
        "function": ""
    },
    7: {
        "name": "Sort by name",
        "function": ""
    },
    8: {
        "name": "Sort by surname",
        "function": ""
    },
    0: {
        "name": "exit",
        "function": "turn_off"
    },
}