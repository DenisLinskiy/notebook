from project.utils.const import MENU
from typing import List
# ----------------------------------------------------------------------------------------------------------------------
#
#    __  __     ______     ______     ______
#   /\ \/\ \   /\  ___\   /\  ___\   /\  == \
#   \ \ \_\ \  \ \___  \  \ \  __\   \ \  __<
#    \ \_____\  \/\_____\  \ \_____\  \ \_\ \_\
#     \/_____/   \/_____/   \/_____/   \/_/ /_/
#
# ----------------------------------------------------------------------------------------------------------------------


class UserInterface:
    @staticmethod
    def send_user(message: str):
        print(message)

    @staticmethod
    def ask_user(message: str) -> str:
        user_data = input(message).strip()
        return user_data


class UserStory(UserInterface):
    def __init__(self):
        pass

    def show_title(self):
        self.send_user("-" * 50)
        self.send_user("MY NOTEBOOK")

    def show_notebook(self):
        self.send_user("-" * 50)
        self.send_user("Your Notebook is empty")

    def show_menu(self):
        self.send_user("-" * 50)
        for user_choice, data in MENU.items():
            print(f"{user_choice}: {data['name']}")
        self.send_user("-" * 50)

    @staticmethod
    def run_command(command: int, notebook):
        getattr(notebook(), MENU[command]["function"])()

    @staticmethod
    def get_avail_items_in_menu() -> List:
        return list(MENU.keys())

