import random

from .user_story import UserStory
from project.utils.helpers import helpers


class NoteBook:
    def __init__(self):
        self.user_story = UserStory()

    def turn_off(self):
        self.user_story.send_user(helpers.wishlist())
        self.user_story.send_user("Bye, bye")
        exit(0)
